import { Router } from 'express';

import healthCheck from '@components/healthcheck/healthCheck.router';
import user from '@components/user/user.router';
import email from '@components/email/email.router';

const router: Router = Router();
router.use(healthCheck);
router.use(user);
router.use(email);

export default router;
