import { Router } from 'express';
import { sendEmail } from './email.controller';


const router: Router = Router()

router.post('/send', sendEmail)

export default router