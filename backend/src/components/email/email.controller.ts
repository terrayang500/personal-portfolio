import { SESv2Client, SendEmailCommand } from "@aws-sdk/client-sesv2";
import { Request, Response } from "express";
import { IContact } from "./email.interface";
import httpStatus from "http-status";
import config from "@config/config";

const client = new SESv2Client({
    region: "us-east-1",
    credentials: {
        accessKeyId: config.access_id,
        secretAccessKey: config.access_secret
    }
})

const sendEmail = async (req: Request, res: Response) => {
    const { name, email, message } = req.body as IContact
    const input = {
        FromEmailAddress: config.from_email,
        Destination: {
            ToAddresses: [
                config.to_email
            ]
        },
        Content: {
            Simple: {
                Subject: {
                    Data: `From ${name}, ${email}`,
                    Charset: "utf-8"
                },
                Body: {
                    Text: {
                        Data: message,
                        Charset: "utf-8"
                    }
                }
            }
        }
        
    }

    const command = new SendEmailCommand(input);
    const response = await client.send(command);
    console.log(response)
    res.status(httpStatus.OK)
    res.send({ message: 'Send' })
}

export { sendEmail }