// Scroll snap to page
(function($) {
  var selector = "section";
  var direction;
  var $slide;
  var offsetTop;
  var $slides = $(selector);
  var currentSlide = 0;
  var isAnimating = false;
  var mobile = false;



  window.addEventListener("resize", () => {
    if (window.matchMedia("(min-width: 500px)").matches) {
      mobile = false;
    } else {
      console.log('asd')
      mobile = true;
    }
  })

  

  $(".main-nav a").on("click", function (e) {
 
    if (isAnimating) {
      e.preventDefault();
      return;
    }
    e.preventDefault();
    // const href = $(this).attr("href");
    const href = $(this).data('number');
    isAnimating = true
    currentSlide = href    
    $slide = $($slides[currentSlide]);
    console.log($slide)
    $("html, body").animate({ scrollTop: $($slide).offset().top }, 500, stopAnimation);
    });

  var stopAnimation = function() {
    setTimeout(function() {
      isAnimating = false;
    }, 100);
  };

  var bottomIsReached = function($elem) {
    var rect = $elem[0].getBoundingClientRect();
    return rect.bottom <= $(window).height();
  };

  var topIsReached = function($elem) {
    var rect = $elem[0].getBoundingClientRect();
    return rect.top >= 0;
  };

  document.addEventListener(
    "wheel",
    function(event) {
      console.log(event)
      var $currentSlide = $($slides[currentSlide]);
      if (isAnimating || mobile == true)   {
        event.preventDefault();
        return;
      }

      direction = -event.deltaY;

      if (direction < 0) {
        // next
        if (currentSlide + 1 >= $slides.length) {
          return;
        }
        if (!bottomIsReached($currentSlide)) {
          return;
        }
        event.preventDefault();
        currentSlide++;
        $slide = $($slides[currentSlide]);
        offsetTop = $slide.offset().top;
        isAnimating = true;
        $("html, body").animate({
            scrollTop: offsetTop
          },
          1000,
          stopAnimation
        );
      } else {
        // back
        if (currentSlide - 1 < 0) {
          return;
        }
        if (!topIsReached($currentSlide)) {
          return;
        }
        event.preventDefault();
        currentSlide--;
        $slide = $($slides[currentSlide]);
        console.log($slides)
        offsetTop = $slide.offset().top;
        isAnimating = true;
        $("html, body").animate({
            scrollTop: offsetTop
          },
          1000,
          stopAnimation
        );
      }
    }, {
      passive: false
    }
  );
})(jQuery);


(function playDropdownMenu () {
  let isDisplayingDropdownMenu = false;
  const dropdown = document.querySelector('.dropdown');
  const dropdownMenu = document.querySelector('.dropdown-menu');
  dropdown.classList.toggle('is-active');
  const t1 = gsap.timeline({ paused: true });
  t1.to(dropdownMenu, {
    "x": 0
  })
  dropdown.addEventListener('click', () => {
    // dropdown.classList.contains('is-active')
    if (isDisplayingDropdownMenu == false) {
      t1.play()
      isDisplayingDropdownMenu = true
    } else {
      t1.reverse()
      isDisplayingDropdownMenu = false
      
    }
  })
})()

// Nav hover animation 
function navHover() {
  let navLink = document.querySelectorAll(".main-nav a");
  
  navLink.forEach(link => {
    link.addEventListener("mouseenter", (e) => {
      gsap.to(e.target, {
        y: "-10px"
      })
    } )
    link.addEventListener("mouseleave", (e) =>{
      gsap.to(e.target, {
        y: "0px"
      })
    })
  }) 
}
navHover()

// Floating animation 
function floatingAnimation() {
  const astronaut = document.querySelector(".astronaut");
  const saturn = document.querySelector(".svg-saturn");
  const tl = gsap.timeline()
  .to(astronaut, {
    x: "random(-20, 20, 2)", //chooses a random number between -20 and 20 for each target, rounding to the closest 5!
    y: "random(-20, 10, 2)",
    duration: 2,
    ease: Sine.easeInOut,
    repeat:-1,
    repeatRefresh:true // gets a new random x and y value on each repeat
  })

  const t2 = gsap.timeline()
  .to(saturn, {
    x: "random(-20, 20, 2)", //chooses a random number between -20 and 20 for each target, rounding to the closest 5!
    y: "random(-20, 10, 2)",
    duration: 2,
    ease: Sine.easeInOut,
    repeat:-1,
    repeatRefresh:true // gets a new random x and y value on each repeat
  }) 
}
floatingAnimation()

// Scroll Trigger
gsap.registerPlugin(ScrollTrigger, TextPlugin)
gsap.to(".project-container", {
  scrollTrigger: {
    trigger: ".project-page",
    toggleActions: "play none none reverse",
    start: "top 40%"
  },
  opacity: 1,
  duration: 1,
  y: 0
})
gsap.to(".skill-category", {
  scrollTrigger: {
    trigger: ".skill-page",
    toggleActions: "play none none reverse",
    start: "top 40%"
  },
  opacity: 1,
  duration: 1,
  y: 0
})
gsap.to(".pug", {
  scrollTrigger: {
    trigger: ".skill-page",
    toggleActions: "play none none reverse",
    start: "top 40%"
  },
  opacity: 1,
  duration: 1,
  y: 0
})

gsap.to(".contact-container", {
  scrollTrigger: {
    trigger: ".contact-page",
    toggleActions: "play none none reverse",
    start: "top 40%"
  },
  opacity: 1,
  duration: 1,
  y: 0
})


// text
textTimeline = gsap.timeline({repeat: -1})
textTimeline.to("h1", {duration: 2, text: "こんにちは！", ease:Linear.easeNone, delay:3}, "japanese")
.to(".intro-1 p", {duration: 2, text: "ヤンチェ　と申します。", ease:Linear.easeNone, delay:3}, "japanese+=1")
.to("h1", {duration: 2, text: "你好!", ease:Linear.easeNone, delay:3}, "chinese")
.to(".intro-1 p", {duration: 2, text: "我是 扬杰", ease:Linear.easeNone, delay:3}, "chinese+=1")
.to("h1", {duration: 2, text: "Hello there!", ease:Linear.easeNone, delay:3}, "english")
.to(".intro-1 p", {duration: 2, text: "Yang Jie here", ease:Linear.easeNone, delay:3}, "english+=1")


// Rocket Animation
const nextPageBtn = document.querySelector('.nextBtn')
let animating = false;
let currentProjPage = 1
let maxPage = 2
nextPageBtn.addEventListener('click', (e) => {
  console.log(animating)
  if (animating == true) return
  animating = true
  let nextPage = currentProjPage + 1
  if (nextPage > maxPage) nextPage = 1
  rocketTimeline = gsap.timeline({onComplete: () => animating = false})
  rocketTimeline.to(".svg-rocket", 0.05, {x:"+=5", yoyo:true, repeat:50})
  .to(".svg-rocket", 0.5, {y:'-1000px' })
  .to(`.page-${currentProjPage}`, {x: '150px', opacity: 0})
  .to(`.page-${currentProjPage}`, {display: "none"})
  .to(`.page-${nextPage}`, {display:"flex"})
  .to(`.page-${nextPage}`, {x: "0px", opacity: 1})
  .to(".svg-rocket", 2, {y:"0px", ease: Bounce.easeOut})
  currentProjPage = nextPage
})

// Pup animation rive

new rive.Rive({
  src: "./assets/image/pup_ui.riv",
  canvas: document.getElementById("canvas"),
  autoplay: true,
  stateMachines: "State Machine 1",
});
// Astronaut animation rive
new rive.Rive({
  src: "./assets/image/astronaut.riv",
  canvas: document.getElementById("loading-canvas"),
  autoplay: true,
  artboard: "Astronaut",
  stateMachines: "State Machine 1"
})

const body = document.querySelector('body')
const html = document.querySelector('html')
body.classList.add('noscroll')
html.classList.add('noscroll')

setTimeout(() => {
  const modal = document.querySelector("div.modal")
  body.classList.remove('noscroll')
  html.classList.remove('noscroll') 
  modal.classList.toggle('is-active')
  
}, 4000)

